# Casino slot game.
A classic casino slot game made as test task for Octavian Games.


## DEMO
https://casino-slot-game-app.herokuapp.com/

## Installation

### `yarn install`
Install dependancies
### `yarn start`
Run development server

## Contacts 
- Telegram: @selean_d
- Email: d.selean@gmail.com