import * as PIXI from "pixi.js";

export const SLOT_MACHINE = {
  REEL_WIDTH: 160,
  SYMBOL_SIZE: 150,
};

export const TEXT_STYLES = {
    MAIN: new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 36,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#4a1850',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
        wordWrapWidth: 440,
    }),
    FPS_COUNTER: new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 20,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: '#ffffff',
        wordWrap: true,
        wordWrapWidth: 440,
    })
};