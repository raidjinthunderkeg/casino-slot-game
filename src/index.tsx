import './index.css';
import * as PIXI from 'pixi.js';
import SlotMachine from "./components/SlotMachine";
import FPSCounter from "./components/FPSCounter";

const app = new PIXI.Application({
    backgroundColor: 0x1099bb
});
document.body.prepend(app.view);

app.loader
    .add('banana.png', 'banana.png')
    .add('cherry.png', 'cherry.png')
    .add('apricot.png', 'apricot.png')
    .add('orange.png', 'orange.png')
    .load( () => {
        const slotMachine = new SlotMachine();
        const fpsCounter = new FPSCounter();

        slotMachine
            .build(app.screen)
            .render(app);

        fpsCounter.render(app);

        app.ticker.add((delta) => {
            slotMachine.update();
        });
    });


