import Reel from "./Reel";
import * as PIXI from "pixi.js";
import {SLOT_MACHINE, TEXT_STYLES} from "../Constants/Constants";
import {backout} from "../Helpers";
import BlinkingButton from "../UI/BlinkingButton";

/**
 * Class for representing the whole Slot Machine.
 * Has a set of reels with several symbols in each.
 */

export default class SlotMachine {

    reelContainer: PIXI.Container;
    reels: Reel[] = [];
    top: PIXI.Graphics = new PIXI.Graphics();
    bottom: PIXI.Graphics = new PIXI.Graphics();

    running: boolean = false;

    constructor(reelsCount: number = 5) {
        this.reelContainer = new PIXI.Container();

        for (let i = 0; i < reelsCount; i++) {
            const reel = new Reel();
            reel
                .addTo(this.reelContainer)
                .setOffsetX(i);

            this.reels.push( reel );
        }
    }

    /**
     * Prepare the elements for being rendered in PIXI.
     * @param appScreen Pixi screen for rendering
     */
    build(appScreen: PIXI.Rectangle) {

        //Build top & bottom covers and position reelContainer
        const margin = (appScreen.height - SLOT_MACHINE.SYMBOL_SIZE * 3) / 2;
        this.reelContainer.y = margin;
        this.reelContainer.x = Math.round(appScreen.width - SLOT_MACHINE.REEL_WIDTH * 5);
        this.top.beginFill(0, 1);
        this.top.drawRect(0, 0, appScreen.width, margin);
        this.bottom.beginFill(0, 1);
        this.bottom.drawRect(0, SLOT_MACHINE.SYMBOL_SIZE * 3 + margin, appScreen.width, margin);

        //Add header text
        const headerText = new PIXI.Text('Fruit slot Machine', TEXT_STYLES.MAIN);
        headerText.x = Math.round((this.top.width - headerText.width) / 2);
        headerText.y = Math.round((margin - headerText.height) / 2);
        this.top.addChild(headerText);

        //Add button
        this.bottom.addChild( new BlinkingButton({
            x: appScreen.width / 2 - 100,
            y: appScreen.height - 62,
            label: 'Roll!',
            width: 200,
            height: 50,
            color: 0x2EF1DA,
            onClick: () => this.roll()
        }) );

        return this;
    }

    /**
     * Render the machine elements to target application
     * @param app Pixi application
     */
    render(app: PIXI.Application) {
        app.stage.addChild(this.reelContainer);
        app.stage.addChild(this.top);
        app.stage.addChild(this.bottom);

        return this;
    }

    /**
     * Update every component of machine.
     */
    update() {
        for (let i = 0; i < this.reels.length; i++) {
            this.reels[i].update();
        }
    }

    /**
     * Initiate the symbols rolling
     */
    roll() {
        if (this.running) return;
        this.running = true;

        for (let i = 0; i < this.reels.length; i++) {
            const r = this.reels[i];
            const target = r.position + 10 + i * 10;
            const time = 1500 + i * 600;
            this.reels[i].tweenTo(
                target,
                time,
                backout(0.3),
                null,
                i === this.reels.length - 1 ? () => this.stop() : null
            );
        }
    }

    /**
     * Stop the symbols roll.
     */
    stop() {
        this.running = false;
    }

    /**
     * Check if machine is currently rolls symbols.
     */
    isRunning() {
        return this.running;
    }
}
