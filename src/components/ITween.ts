export default interface ITween {
    startingPosition: number,
    change: ((t: ITween) => void) | null,
    complete: ((t: ITween) => void) | null,
    target: any,
    time: number,
    easing: (t: number) => number,
    start: number
}