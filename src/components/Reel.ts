import * as PIXI from "pixi.js";
import SlotSymbol from "./SlotSymbol";
import {SLOT_MACHINE} from "../Constants/Constants";
import {lerp} from "../Helpers";
import ITween from "./ITween";

//Class for representing the single Reel of slot machine
export default class Reel {

    container: PIXI.Container = new PIXI.Container();
    symbols: SlotSymbol[] = [];
    position: number = 0;
    previousPosition: number = 0;
    blur: PIXI.filters.BlurFilter = new PIXI.filters.BlurFilter();

    isTweening: boolean = false;
    tween: ITween = {
        startingPosition: 0,
        change: () => {},
        complete: () => {},
        target: 0,
        time: 0,
        easing: (t: number) => 0,
        start: 0
    };

    constructor(public width: number = SLOT_MACHINE.REEL_WIDTH, symbolsCount: number = 4 ) {

        this.blur.blurX = 0;
        this.blur.blurY = 0;
        this.container.filters = [this.blur];

        // Build the symbols
        for (let i = 0; i < symbolsCount; i++) {
            const slotSymbol = new SlotSymbol();
            slotSymbol
                .addTo(this.container)
                .setOffsetY(i);

            this.symbols.push(slotSymbol);
        }

    }

    addTo(container: PIXI.Container) {
        container.addChild(this.container);
        return this;
    }

    setOffsetX(reelCounter: number) {
        this.container.x = reelCounter * this.width;
        return this;
    }

    update() {
        this.blur.blurY = (this.position - this.previousPosition) * 8;
        this.previousPosition = this.position;

        for (let j = 0; j < this.symbols.length; j++) {
            this.symbols[j].update(this.position + j, this.symbols.length);
        }

        if( this.isTweening ) {
            this.updateTween();
        }
    }

    updateTween() {
        const now = Date.now();
        const t = this.tween;
        const phase = Math.min(1, (now - t.start) / t.time);

        this.position = lerp(t.startingPosition, t.target, t.easing(phase));
        if (t.change) t.change(t);
        if (phase === 1) {
            this.position = t.target;
            if (t.complete) t.complete(t);
            this.isTweening = false;
        }
    }

    /**
     * Run tweening animation
     * @param target Target position
     * @param time Time of animation in milliseconds
     * @param easing Easing function
     * @param onchange Callback for every frame update
     * @param oncomplete Callback for animation completion
     */
    tweenTo(
        target: any,
        time: number,
        easing: (t: number) => number,
        onchange: (() => void) | null,
        oncomplete: (() => void) | null
    ) {
        const tween = {
            startingPosition: this.position,
            target,
            easing,
            time,
            change: onchange,
            complete: oncomplete,
            start: Date.now(),
        };
        this.tween = tween;
        this.isTweening = true;
        return tween;
    }
}