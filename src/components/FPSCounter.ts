import * as PIXI from 'pixi.js';
import {TEXT_STYLES} from "../Constants/Constants";


export default class FPSCounter {
    private _textField: PIXI.Text;
    private _fpsTicker: PIXI.Ticker;

    private _timeValues: number[];
    private _lastTime: number;

    constructor() {
        this._textField = new PIXI.Text("", TEXT_STYLES.FPS_COUNTER);

        this._timeValues = [];
        this._lastTime = new Date().getTime();

        this._fpsTicker = new PIXI.Ticker();
        this._fpsTicker.add(() => {
            this.measureFPS();
        });
        this._fpsTicker.start();
    }

    private measureFPS(): void {
        const currentTime = new Date().getTime();
        this._timeValues.push(1000 / (currentTime - this._lastTime));

        if (this._timeValues.length === 30) {
            let total = 0;
            for (let i = 0; i < 30; i++) {
                total += this._timeValues[i];
            }

            this._textField.text = 'FPS: ' + (total / 30).toFixed(0);

            this._timeValues.length = 0;
        }

        this._lastTime = currentTime;
    }

    render(app: PIXI.Application) {
        this._textField.y = app.screen.height - 20;
        app.stage.addChild(this._textField);
    }
}