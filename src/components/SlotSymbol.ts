import * as PIXI from "pixi.js";
import {getRandomSlotTexture} from "../Helpers";
import {SLOT_MACHINE} from "../Constants/Constants";

//Class for representing the single symbol in reel of slot machine
export default class SlotSymbol {
    sprite: PIXI.Sprite;

    constructor(public size: number = SLOT_MACHINE.SYMBOL_SIZE) {
        const sprite = new PIXI.Sprite(getRandomSlotTexture());

        // Scale the symbol to fit symbol area.
        sprite.scale.x = sprite.scale.y = Math.min(size / sprite.width, size / sprite.height);
        sprite.x = Math.round((size - sprite.width) / 2);

        this.sprite = sprite;
    }

    addTo(container: PIXI.Container) {
        container.addChild(this.sprite);
        return this;
    }

    setOffsetY(counter: number) {
        this.sprite.y = counter * this.size;
        return this;
    }

    update(position: number, symbolsLength: number) {
        const s = this.sprite;
        const prevy = s.y;
        s.y = (position % symbolsLength) * SLOT_MACHINE.SYMBOL_SIZE - SLOT_MACHINE.SYMBOL_SIZE;
        if (s.y < 0 && prevy > SLOT_MACHINE.SYMBOL_SIZE) {
            // Detect going over and swap a texture.
            // This should in proper product be determined from some logical reel.
            s.texture = getRandomSlotTexture();
            s.scale.x = s.scale.y = Math.min(
                SLOT_MACHINE.SYMBOL_SIZE / s.texture.width,
                SLOT_MACHINE.SYMBOL_SIZE / s.texture.height
            );
            s.x = Math.round((SLOT_MACHINE.SYMBOL_SIZE - s.width) / 2);
        }
    }
}