import * as PIXI from "pixi.js";


export function getRandomSlotTexture() {
    const textures = [
        PIXI.Texture.from('banana.png'),
        PIXI.Texture.from('cherry.png'),
        PIXI.Texture.from('apricot.png'),
        PIXI.Texture.from('orange.png'),
    ];

    return textures[Math.floor(Math.random() * textures.length)];
}


// Basic lerp funtion.
export function lerp(a1: number, a2: number, t: number) {
    return a1 * (1 - t) + a2 * t;
}

// Backout function from tweenjs.
// https://github.com/CreateJS/TweenJS/blob/master/src/tweenjs/Ease.js
export function backout(amount: number) {
    return (t: number) => (--t * t * ((amount + 1) * t + amount) + 1);
}