import Button from "./Button";
import * as PIXI from "pixi.js";

export default class BlinkingButton extends Button {
    blink: PIXI.Graphics;

    constructor(options: {
        label?: string,
        x?: number,
        y?: number,
        width?: number,
        height?: number,
        onClick?: () => void,
        color?: number,
        blinkInterval?: number,
    }) {
        super(options);
        const {
            x               = 0,
            y               = 0,
            width           = 200,
            height          = 100,
            color           = 0xFF5733,
            blinkInterval   = 500,
        } = options;

        const blink = new PIXI.Graphics();
        blink.beginFill(color, 1);
        blink.drawRoundedRect(x, y, width, height, 16);
        blink.endFill();
        this.blink = blink;
        this.addChild(blink);

        this._runBlinking(blinkInterval);
        this.addButtonLabel();
    }

    _runBlinking(interval: number) {
        let blinked = false;
        setInterval( () => {
            if( blinked ) {
                this.blink.alpha = 0;
            } else {
                this.blink.alpha = 1;
            }
            blinked = !blinked;
        }, interval);
    }
}