import * as PIXI from "pixi.js";
import {TEXT_STYLES} from "../Constants/Constants";

export default class Button extends PIXI.Graphics {

    buttonLabel: PIXI.Text;

    constructor(options: {
        label?: string,
        x?: number,
        y?: number,
        width?: number,
        height?: number,
        color?: number,
        onClick?: () => void
    }) {
        super();
        const {
            x       = 0,
            y       = 0,
            width   = 200,
            height  = 100,
            label   = 'Button',
            color   = 0xFF5733,
            onClick = () => console.error('On click handler not specified.')
        } = options;

        this.lineStyle(2, 0x1A2A28, 1);
        this.beginFill(color, 0.5);
        this.drawRoundedRect(x, y, width, height, 16);
        this.endFill();

        const btnLabel = new PIXI.Text(label, TEXT_STYLES.MAIN);
        btnLabel.x = x + width / 2 - btnLabel.width / 2;
        btnLabel.y = y + height / 2 - btnLabel.height / 2;
        this.buttonLabel = btnLabel;

        this.interactive = true;
        this.buttonMode = true;

        this.addListener('pointerdown', () => {
            onClick();
        });

    }

    addButtonLabel() {
        this.addChild(this.buttonLabel);
    }
}